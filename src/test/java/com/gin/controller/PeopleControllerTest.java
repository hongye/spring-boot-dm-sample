package com.gin.controller;

import com.gin.BaseTest;
import com.gin.entity.People;
import com.gin.enums.ResultCodeEnum;
import com.gin.service.PeopleService;
import com.gin.utils.result.Result;
import java.util.List;
import javax.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
public class PeopleControllerTest extends BaseTest {

  private MockMvc mockMvc;

  @Resource
  private PeopleService peopleService;

  @Resource
  private PeopleController peopleController;


  @Test
  public void testAddPeople() throws Exception {
    // 创建一个People对象用于测试
    People people = new People();
    people.setName("John Doe");
    people.setAge(30);

    Result<Void> result = peopleController.addPeople(people);

    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNull(result.getData());
  }

  @Test
  public void testListPeople() throws Exception {

    Result<List<People>> result = peopleController.list();

    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNotNull(result.getData());
  }
}