package com.gin.controller;


import com.gin.BaseTest;
import com.gin.entity.Message;
import com.gin.enums.ResultCodeEnum;
import com.gin.service.MessageService;
import com.gin.utils.result.Result;
import java.time.LocalDateTime;
import javax.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MessageControllerTest extends BaseTest {


  @Resource
  private MessageController messageController;

  @Resource
  private MessageService messageService;

  @Test
  void testAdd() {
    Message message = new Message();
    message.setContent("Test message");
    message.setSendAt(LocalDateTime.now());

    Result<Void> result = messageController.add(message);

    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNull(result.getData());
  }
}