package com.gin.controller;

import com.gin.BaseTest;
import com.gin.entity.Company;
import com.gin.utils.result.Result;
import com.gin.utils.result.ResultUtil;
import javax.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class CompanyControllerTest extends BaseTest {

  @Resource
  private CompanyController companyController;


  @Test
  void testAddCompany() {
    Company company = new Company();
    company.setId(1L);
    company.setCompanyName("Test Company");

    Result<Void> result = companyController.addPeople(company);

    Assertions.assertEquals(ResultUtil.ok().getCode(), result.getCode());
    Assertions.assertNull(result.getData());
  }
}