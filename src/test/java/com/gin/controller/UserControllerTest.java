package com.gin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDTO;
import com.gin.BaseTest;
import com.gin.entity.User;
import com.gin.enums.ResultCodeEnum;
import com.gin.enums.SexEnum;
import com.gin.utils.result.Result;
import com.gin.vo.UserVo;
import java.util.Date;
import javax.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserControllerTest extends BaseTest {

  @Resource
  private UserController userController;

  @Test
  void addUser() {
    User user = new User();
    user.setAccount("张飞");
    user.setFirstLogin(new Date());
    user.setPhone("13223453456");
    user.setSex(SexEnum.WOMEN);
    user.setDeleted(0);

    Result<Void> result = userController.addUser(user, null);
    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNull(result.getData());
  }

  @Test
  void updateById() {
    User user = new User();
    user.setId(1L);
    user.setAccount("testUser-new");
    user.setFirstLogin(new Date());
    user.setPhone("12345678901");
    user.setSex(SexEnum.MAN);
    Result<Void> result = userController.updateById(user);
    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNull(result.getData());
  }

  @Test
  void update() {
    User user = new User();
    user.setId(1L);
    user.setAccount("testUser-2");
    user.setFirstLogin(new Date());
    user.setPhone("12345678901");
    user.setSex(SexEnum.MAN);
    Result<Void> result = userController.update(user);
    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
  }

  @Test
  void updateExclude() {
    User user = new User();
    user.setId(1L);
    user.setAccount("testUser-updateExclude");
    user.setFirstLogin(new Date());
    user.setPhone("12345678901");
    user.setSex(SexEnum.MAN);
    user.setDeleted(0);

    Result<Void> result = userController.updateExclude(user);
    Assertions.assertTrue(result.getCode() == ResultCodeEnum.SUCCESS.getValue());
  }

  @Test
  void updateSelectiveByPrimaryKey() {
  }

  @Test
  void detail() {
    Result<User> detail = userController.detail(1L, true);
    System.out.println(detail.getData());
    Assertions.assertTrue(detail.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNotNull(detail.getData());
  }

  @Test
  void delete() {
    Result<Void> delete = userController.delete(1L);
    Assertions.assertTrue(delete.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNull(delete.getData());
  }

  @Test
  void getPage() {
    Result<IPage<User>> page = userController.getPage(null);
    page.getData().getRecords().forEach(System.out::println);
    Assertions.assertTrue(page.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNotNull(page.getData());
  }

  @Test
  void getPag2() {
    PageDTO pageDTO = new PageDTO();
    pageDTO.setSize(10);
    pageDTO.setCurrent(0);

    Result<IPage<UserVo>> page = userController.getPag2(pageDTO, "test");
    page.getData().getRecords().forEach(System.out::println);
    Assertions.assertTrue(page.getCode() == ResultCodeEnum.SUCCESS.getValue());
    Assertions.assertNotNull(page.getData());
//    Assertions.assertNotNull(page.getData().getRecords().get(0).getPeopleName());
  }
}