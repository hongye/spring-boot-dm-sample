## 项目介绍

一个在spring boot 项目中使用 达梦V8 示例代码，ormapping 框架采用的是 mybatis-plugs

## 启动方式

1. 在达梦数据库中创建表空间、用户。（创建好后，系统会生产默认的schema）
2. 在步骤一系统生成的 schema 中执行 init.sql
3. 修改项目配置文件，使用你的数据库账号、密码
4. 执行单元测试
